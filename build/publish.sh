#!/usr/bin/env bash

set -euo pipefail

for image_file in artifacts/final/*.txt; do
  source_image="$(cat "${image_file}")"
  echo "Artifact '${image_file}' lists '${source_image}'"

  source_image_tag="${source_image##*:}"
  source_image_tag_suffix="${source_image_tag##*-}"
  if [ "$source_image_tag_suffix" = "$source_image_tag" ]; then
      source_image_tag_suffix=""
  fi

  source_image_repo="$(skopeo inspect "docker://${source_image}" --format='{{ .Name }}')"
  source_image_name="${source_image_repo##*/}"

  # Construct the new image repository name
  # shellcheck disable=SC2154
  target_image_with_commit_tag="${CI_REGISTRY_IMAGE}/${source_image_name}:${source_image_tag}"
  target_image_with_version_tag="${CI_REGISTRY_IMAGE}/${source_image_name}:${TAG}${source_image_tag_suffix:+-$source_image_tag_suffix}"

  # shellcheck disable=SC2154
  if [[ ${TEST_PUBLISH} == true ]]; then
    echo "DRY RUN - Would run: skopeo copy \"docker://${source_image}\" \"docker://${target_image_with_commit_tag}\""
    echo "DRY RUN - Would run: skopeo copy \"docker://${source_image}\" \"docker://${target_image_with_version_tag}\""
  else
    skopeo copy "docker://${source_image}" "docker://${target_image_with_commit_tag}"
    skopeo copy "docker://${source_image}" "docker://${target_image_with_version_tag}"
  fi
done
