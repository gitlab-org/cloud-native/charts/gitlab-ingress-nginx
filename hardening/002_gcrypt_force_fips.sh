#!/bin/sh
set -e

if [ "$FIPS_MODE" -eq 1 ]; then

(>&2 echo "Configuration: Setting gcrypt/libcrypt component to enforce FIPS")

## Enforce gcrypt/libcrypt (GnuPG)
# https://www.gnupg.org/documentation/manuals/gcrypt/Enabling-FIPS-mode.html
mkdir -p /etc/gcrypt
printf 1 > /etc/gcrypt/fips_enabled

fi