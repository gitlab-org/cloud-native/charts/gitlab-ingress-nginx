# Based off of ./.github/workflows/ci.yaml

stages:
  - build
  - test
  - certification
  - release

include:
  - project: 'gitlab-org/cloud-native/preflight'
    file: '/pipeline-template/certify.yaml'

variables:
  TEST_PUBLISH: "false"
  DOCKER_VERSION: 27.4.1

workflow:
  rules:
    - if: '($CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH) || ($CI_PIPELINE_SOURCE == "schedule")'
      variables:
        TEST_PUBLISH: "true"
    - when: always

build:
  stage: build
  image: docker:${DOCKER_VERSION}
  timeout: 2 hours
  services:
    - name: docker:${DOCKER_VERSION}-dind
      variables:
        TEST_INTEGRATION_USE_SNAPSHOTTER: "true"
  before_script:
    - apk add --no-cache git make musl-dev go bash qemu qemu-img qemu-system-x86_64 qemu-ui-gtk
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script: make release
  variables:
    DOCKER_DRIVER: overlayfs
    # Override default platforms list to only build `amd64` for now
    PLATFORMS:  amd64
    BUILDX_PLATFORMS: linux/amd64
    # Override default registry path
    REGISTRY: "${CI_REGISTRY_IMAGE}/staging"
    # Override BUILD_INFO to ensure it doesn't contain CI token
    REPO_INFO: https://gitlab.com/gitlab-org/cloud-native/charts/gitlab-ingress-nginx
    # Override tag when building
    TAG: "${CI_COMMIT_SHORT_SHA}"
    # Allow testing the release stage without actually publishing
  artifacts:
    paths:
      - artifacts/final/

# Source:
# https://gitlab.com/gitlab-org/build/CNG/-/blob/befa8e490d798a93a23a377116520a2357eb425a/.gitlab/ci/images.gitlab-ci.yml#L101
container_scanning:
  stage: test
  allow_failure: true
  script:
    # Debugging information
    - UBI_IMAGE=$(ARCH=amd64 REGISTRY="${CI_REGISTRY_IMAGE}" make show-ubi-base-image-version)
    - echo "UBI image version ${UBI_IMAGE}"
    - for i in artifacts/final/* ; do echo "${i} - $(cat ${i})" ; done
    # Populate the `images` variable contents
    - images=$(cat artifacts/final/* | xargs printf "%s,")
    # Trigger the scan
    - curl -fS
        --request POST
        --form ref=master
        --form "variables[IMAGES]=$images"
        --form "variables[CONTAINER_DEDUPE_MODEL]=cng/ubi.yaml"
        --form "token=${SCANNING_TRIGGER_TOKEN}"
        https://gitlab.com/api/v4/projects/16505542/trigger/pipeline
  rules:
    - if: '$CI_COMMIT_TAG'
      allow_failure: true
    # If we're testing the publish functionality
    - if: '$TEST_PUBLISH == "true"'
      allow_failure: true

certification:
  extends: .preflight-certification
  stage: certification
  variables:
    CERTIFY_IMAGE: controller
  before_script:
    - export CERTIFY_TAG=$(cat ./TAG)-ubi
  rules:
    # If on default branch
    - if: '($CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH)'
    # If we're testing the publish functionality
    - if: '$TEST_PUBLISH == "true"'

release:
  stage: release
  image: docker:${DOCKER_VERSION}
  services:
    - docker:${DOCKER_VERSION}-dind
  before_script:
    - apk add --no-cache bash skopeo
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - export TAG=$(cat TAG)
    - bash build/publish.sh
  rules:
    # If on default branch
    - if: '($CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH)'
      # Only when triggered manually
      when: manual
    # If we're testing the publish functionality
    - if: '$TEST_PUBLISH == "true"'

